#!/usr/bin/env python
# coding: utf-8

# # Chernobyl Chemical Radiation Analysis
# #By- Aarush Kumar
# #Dated: July 20,2021

# In[1]:


from IPython.display import Image
Image(url='https://cdn.theatlantic.com/media/img/photo/2019/06/chernobyl-disaster-photos-1986/c02_629912323/main_1500.jpg')


# In[2]:


get_ipython().system('pip install dataprep by')


# In[3]:


get_ipython().system('pip install -U matplotlib==3.2')


# In[4]:


get_ipython().system('pip install yellowbrick')


# In[5]:


get_ipython().system('pip install pymc3')


# In[6]:


get_ipython().system('pip install statsmodels ')


# In[7]:


get_ipython().system('pip install missingno')


# In[8]:


get_ipython().system('pip install lightgbm')


# In[9]:


get_ipython().system('pip install catboost')


# In[10]:


import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from warnings import filterwarnings
from mpl_toolkits.mplot3d import Axes3D
import statsmodels.api as sm
import missingno as msno
import statsmodels.stats.api as sms
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
from sklearn.neighbors import LocalOutlierFactor
from scipy.stats import levene
from scipy.stats import shapiro
from scipy.stats.stats import pearsonr
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, cross_val_score, cross_val_predict
from sklearn.preprocessing import scale
from sklearn.model_selection import ShuffleSplit, GridSearchCV
from sklearn.metrics import mean_squared_error, r2_score
from sklearn import model_selection
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import BaggingRegressor
from sklearn.svm import SVR
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPRegressor
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LinearRegression
from sklearn.cross_decomposition import PLSRegression
from sklearn.linear_model import Ridge
from sklearn.linear_model import RidgeCV
from sklearn.linear_model import Lasso
from sklearn.linear_model import LassoCV
from sklearn.linear_model import ElasticNet
from sklearn.linear_model import ElasticNetCV
from sklearn import linear_model
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.ensemble import GradientBoostingRegressor, GradientBoostingClassifier
import xgboost as xgb
from xgboost import XGBRegressor, XGBClassifier
from lightgbm import LGBMRegressor, LGBMClassifier
from catboost import CatBoostRegressor, CatBoostClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn import tree
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report, roc_auc_score, roc_curve
from yellowbrick.cluster import KElbowVisualizer
from sklearn.cluster import KMeans
from sklearn.pipeline import Pipeline
from sklearn.manifold import Isomap,TSNE
from sklearn.feature_selection import mutual_info_classif
from tqdm.notebook import tqdm
from scipy.stats import ttest_ind
import plotly.express as px
import plotly.graph_objs as go
import plotly.offline as pyo
import scipy.stats as stats
import pymc3 as pm
from dataprep.eda import *
from dataprep.eda import plot
from dataprep.eda import plot_diff
from dataprep.eda import plot_correlation
from dataprep.eda import plot_missing
import plotly.figure_factory as ff
from collections import Counter
import pandas_profiling as pp
from mpl_toolkits.basemap import Basemap
import datetime as dt
import plotly.express as px
import plotly.graph_objects as go


# In[11]:


Chernobyl_CSV = pd.read_csv("/home/aarush100616/Downloads/Projects/Chernobyl Chemical Radiation Analysis/Chernobyl_ Chemical_Radiation.csv")
Data = Chernobyl_CSV.copy()


# In[12]:


Data


# In[13]:


Data.info()


# In[14]:


Data.isnull().sum()


# In[15]:


Data.describe()


# In[16]:


Data.columns


# In[17]:


Data.shape


# In[18]:


Data.size


# In[19]:


Data.rename(columns={"I_131_(Bq/m3)":"Iodine_131",
                    "Cs_134_(Bq/m3)":"Caesium_134",
                    "Cs_137_(Bq/m3)":"Caesium_137"},inplace=True)


# In[20]:


print("COLUMNS:\n")
print(Data.columns)


# In[21]:


Data.replace({'AU': 'Austria',
                       'BE': 'Belgium',
                       'CH': 'Switzerland',
                       'CZ': 'Czechoslovakia',
                       'DE': 'Germany',
                       'ES': 'Spain',
                       'F': 'France',
                       'FI': 'Finland',
                       'GR': 'Greece',
                       'HU': 'Hungary',
                       'IR': 'Ireland',
                       'IT': 'Italy',
                       'NL': 'Netherlands',
                       'NO': 'Norway',
                       'SE': 'Sweden',
                       'UK': 'United Kingdom'},inplace=True)


# In[22]:


print("PAYS VALUES:\n")
print(Data["PAYS"].value_counts())


# In[23]:


Data.drop(["Code"],axis=1,inplace=True)


# In[24]:


Data[["Iodine_131","Caesium_134","Caesium_137"]] = Data[["Iodine_131","Caesium_134","Caesium_137"]].replace("<",np.NaN)
Data[["Iodine_131","Caesium_134","Caesium_137"]] = Data[["Iodine_131","Caesium_134","Caesium_137"]].replace("L",np.NaN)
Data[["Iodine_131","Caesium_134","Caesium_137"]] = Data[["Iodine_131","Caesium_134","Caesium_137"]].replace("N",np.NaN)


# In[25]:


Data[["Iodine_131","Caesium_134","Caesium_137"]] = Data[["Iodine_131","Caesium_134","Caesium_137"]].apply(pd.to_numeric)


# In[26]:


Data.info()


# In[27]:


Data.sort_values(by=["Date"],inplace=True)


# In[28]:


Data


# In[29]:


Data["Date"] = pd.to_datetime(Data["Date"])


# In[30]:


Data.info()


# In[31]:


Data["Date"]


# In[32]:


Index_Data = Data.copy()


# In[33]:


Index_Data.set_index(["Location","Date"],inplace=True)


# In[34]:


Index_Data


# In[35]:


Average_Data = Index_Data.copy()


# In[36]:


Average_Data = Index_Data.groupby(["Location","Date","PAYS"],sort=False).mean()


# In[37]:


Average_Data


# In[38]:


Average_Data = Average_Data.interpolate(method="linear",axis=0)


# In[39]:


print("NAN VALUES:\n")
print(Average_Data.isna().sum())


# In[40]:


Average_Data


# In[41]:


Numeric_Data = Data.select_dtypes(include=["float32","float64","int32","int64","uint8"])


# In[42]:


Numeric_Data = Numeric_Data.reset_index(drop=True)


# In[43]:


Numeric_Data


# In[44]:


plt.style.use("dark_background")


# In[45]:


msno.matrix(Data,figsize=(15,5))
plt.show()


# In[46]:


msno.bar(Data,figsize=(15,5))
plt.show()


# In[47]:


msno.dendrogram(Data,figsize=(15,5))
plt.show()


# In[48]:


msno.heatmap(Data,figsize=(15,5))
plt.show()


# In[49]:


figure = plt.figure(figsize=(15,5))
plt.title("NAN")
NAN_CHECKING_Data = Data.isna().sum().sort_values(ascending=False).to_frame()
sns.heatmap(NAN_CHECKING_Data,fmt="d",cmap="hot")
plt.show()


# In[50]:


Inter_Data = Data.interpolate(method="linear",axis=0)


# In[51]:


print("NAN VALUES:\n")
print(Inter_Data.isna().sum())


# In[52]:


Inter_Data = Inter_Data.reset_index(drop=True)


# In[53]:


print("INFO:\n")
print(Inter_Data.info())


# In[54]:


msno.bar(Inter_Data,figsize=(15,5))
plt.show()


# In[55]:


Mean_Data = Data.copy()


# In[56]:


Mean_Data["Iodine_131"].fillna(Mean_Data.groupby(["PAYS"])["Iodine_131"].transform("mean"),inplace=True)
Mean_Data["Caesium_134"].fillna(Mean_Data.groupby(["PAYS"])["Caesium_134"].transform("mean"),inplace=True)
Mean_Data["Caesium_137"].fillna(Mean_Data.groupby(["PAYS"])["Caesium_137"].transform("mean"),inplace=True)


# In[57]:


print("NAN VALUES:\n")
print(Mean_Data.isna().sum())


# In[58]:


Mean_Data.dropna(inplace=True)


# In[59]:


print("NAN VALUES:\n")
print(Mean_Data.isna().sum())


# In[60]:


Mean_Data = Mean_Data.reset_index(drop=True)


# In[61]:


print("INFO:\n")
print(Mean_Data.info())


# In[62]:


Mean_Data


# In[63]:


msno.bar(Mean_Data,figsize=(15,5))
plt.show()


# In[64]:


Categorical_Data = Mean_Data.copy()


# In[65]:


Categorical_Data["PAYS"] = pd.Categorical(Categorical_Data["PAYS"])
Categorical_Data["Location"] = pd.Categorical(Categorical_Data["Location"])


# In[66]:


Categorical_Data.sort_values(by=["Date"],inplace=True)


# In[67]:


print("INFO:\n")
print(Categorical_Data.info())


# ## EXPLORATORY DATA ANALYSIS (EDA)¶

# In[68]:


Mean_Corr_Pearson = Mean_Data.corr(method="pearson")
Mean_Corr_Spearman = Mean_Data.corr(method="spearman")


# In[69]:


Inter_Corr_Pearson = Inter_Data.corr(method="pearson")
Inter_Corr_Spearman = Inter_Data.corr(method="spearman")


# In[70]:


figure = plt.figure(figsize=(16,10))
plt.title("MEAN PEARSON")
sns.heatmap(Mean_Corr_Pearson,annot=True,vmin=-1,center=0,vmax=1,linewidths=2,linecolor="black",cmap="jet")
plt.show()


# In[71]:


figure = plt.figure(figsize=(16,10))
plt.title("MEAN SPEARMAN")
sns.heatmap(Mean_Corr_Spearman,annot=True,vmin=-1,center=0,vmax=1,linewidths=2,linecolor="black",cmap="jet")
plt.show()


# In[72]:


figure = plt.figure(figsize=(16,10))
plt.title("INTER PEARSON")
sns.heatmap(Inter_Corr_Pearson,annot=True,vmin=-1,center=0,vmax=1,linewidths=2,linecolor="black",cmap="jet")
plt.show()


# In[73]:


figure = plt.figure(figsize=(16,10))
plt.title("INTER SPEARMAN")
sns.heatmap(Inter_Corr_Spearman,annot=True,vmin=-1,center=0,vmax=1,linewidths=2,linecolor="black",cmap="jet")
plt.show()


# In[74]:


Cov_Inter = Inter_Data.cov()
Cov_Mean = Mean_Data.cov()


# In[75]:


figure = plt.figure(figsize=(16,10))
plt.title("MEAN COVARIANCE")
sns.heatmap(Cov_Mean,annot=True,vmin=-1,center=0,vmax=1,linewidths=2,linecolor="black",cmap="jet")
plt.show()


# In[76]:


figure = plt.figure(figsize=(16,10))
plt.title("INTER COVARIANCE")
sns.heatmap(Cov_Inter,annot=True,vmin=-1,center=0,vmax=1,linewidths=2,linecolor="black",cmap="jet")
plt.show()


# In[77]:


print("Iodine_131 MEAN:\n")
print(Inter_Data["Iodine_131"].mean())
print("\n")
print("Iodine_131 MIN:\n")
print(Inter_Data["Iodine_131"].min())
print("\n")
print("Iodine_131 MAX:\n")
print(Inter_Data["Iodine_131"].max())


# In[78]:


print("Caesium_134 MEAN:\n")
print(Inter_Data["Caesium_134"].mean())
print("\n")
print("Caesium_134 MIN:\n")
print(Inter_Data["Caesium_134"].min())
print("\n")
print("Caesium_134 MAX:\n")
print(Inter_Data["Caesium_134"].max())


# In[79]:


print("Caesium_137 MEAN:\n")
print(Inter_Data["Caesium_137"].mean())
print("\n")
print("Caesium_137 MIN:\n")
print(Inter_Data["Caesium_137"].min())
print("\n")
print("Caesium_137 MAX:\n")
print(Inter_Data["Caesium_137"].max())


# In[80]:


print("DATE MIN:\n")
print(Inter_Data["Date"].min())
print("\n")
print("DATE MAX:\n")
print(Inter_Data["Date"].max())


# In[82]:


figure = plt.figure(figsize=(25,10))
plt.hist(Inter_Data[Inter_Data["Caesium_134"] >= Inter_Data["Caesium_134"].mean()]["PAYS"],linestyle=('dashed'),color=('red'))
plt.title("MORE DANGEROUS THAN OTHERS (Caesium_134)")
plt.show()


# In[83]:


figure = plt.figure(figsize=(25,10))
plt.hist(Inter_Data[Inter_Data["Caesium_137"] >= Inter_Data["Caesium_137"].mean()]["PAYS"],linestyle=('dashed'),color=('yellow'))
plt.title("MORE DANGEROUS THAN OTHERS (Caesium_137)")
plt.show()


# In[84]:


figure = plt.figure(figsize=(25,10))
plt.hist(Inter_Data[Inter_Data["Iodine_131"] >= Inter_Data["Iodine_131"].mean()]["PAYS"],linestyle=('dashed'),color=('green'))
plt.title("MORE DANGEROUS THAN OTHERS (Iodine_131)")
plt.show()


# In[85]:


Inter_Data["Total"] = Inter_Data["Iodine_131"] + Inter_Data["Caesium_134"] + Inter_Data["Caesium_137"]


# In[86]:


Inter_Data


# In[87]:


figure = plt.figure(figsize=(25,10))
plt.hist(Inter_Data[Inter_Data["Total"] >= Inter_Data["Total"].mean()]["PAYS"],linestyle=('dashed'),color=('blue'))
plt.title("MORE DANGEROUS THAN OTHERS (Total)")
plt.show()


# In[88]:


print("MEAN COUNTRY:\n")
print(Inter_Data.groupby(["PAYS"])["Caesium_134","Caesium_137","Iodine_131"].mean())


# In[89]:


print("MEAN LOCATIONS:\n")
print(Inter_Data.groupby(["Location"])["Caesium_134","Caesium_137","Iodine_131"].mean())


# In[90]:


figure = plt.figure(figsize=(25,10))
plt.hist(Inter_Data["Caesium_134"],color="red")
plt.title("Caesium_134")
plt.show()


# In[91]:


figure = plt.figure(figsize=(25,10))
plt.hist(Inter_Data["Caesium_137"],color="yellow")
plt.title("Caesium_137")
plt.show()


# In[92]:


figure = plt.figure(figsize=(25,10))
plt.hist(Inter_Data["Iodine_131"],color="green")
plt.title("Iodine_131")
plt.show()


# In[93]:


figure = plt.figure(figsize=(25,10))
plt.hist(Inter_Data["Total"],color="blue")
plt.title("Total")
plt.show()


# In[94]:


figure = plt.figure(figsize=(25,10))
plt.hist(Inter_Data["PAYS"])
plt.title("Country")
plt.show()


# In[95]:


figure = plt.figure(figsize=(20,8))
sns.distplot(Inter_Data["Caesium_134"] >= Inter_Data["Caesium_134"].mean(), color='red',label='Caesium_134')
plt.title('MEAN', fontsize=10)
plt.legend()
plt.show()


# In[96]:


figure = plt.figure(figsize=(20,8))
sns.distplot(Inter_Data["Caesium_137"] >= Inter_Data["Caesium_137"].mean(), color='yellow',label='Caesium_137')
plt.title('MEAN', fontsize=10)
plt.legend()
plt.show()


# In[97]:


figure = plt.figure(figsize=(20,8))
sns.distplot(Inter_Data["Iodine_131"] >= Inter_Data["Iodine_131"].mean(), color='green',label='Iodine_131')
plt.title('MEAN', fontsize=10)
plt.legend()
plt.show()


# In[98]:


figure = plt.figure(figsize=(20,8))
plt.plot(Inter_Data.groupby(["PAYS"])["Caesium_134"].mean(),label="Caesium_134")
plt.plot(Inter_Data.groupby(["PAYS"])["Caesium_137"].mean(),label="Caesium_137")
plt.plot(Inter_Data.groupby(["PAYS"])["Iodine_131"].mean(),label="Iodine_131")
plt.title('COUNTRY', fontsize=10)
plt.legend()
plt.show()


# In[99]:


figure = plt.figure(figsize=(20,8))
plt.plot(Inter_Data["Date"],Inter_Data["Caesium_134"])
plt.title('Caesium_134', fontsize=10)
plt.legend()
plt.show()


# In[100]:


figure = plt.figure(figsize=(20,8))
plt.plot(Inter_Data["Date"],Inter_Data["Caesium_137"])
plt.title('Caesium_137', fontsize=10)
plt.legend()
plt.show()


# In[102]:


figure = plt.figure(figsize=(20,8))
plt.plot(Inter_Data["Date"],Inter_Data["Iodine_131"])
plt.title('Iodine_131', fontsize=10)
plt.legend()
plt.show()


# In[103]:


figure = plt.figure(figsize=(20,8))
plt.plot(Inter_Data["Date"],Inter_Data["Total"])
plt.title('Total', fontsize=10)
plt.legend()
plt.show()


# In[104]:


figure = plt.figure(figsize=(22,8))
sns.scatterplot(x=Inter_Data["PAYS"],y=Inter_Data["Caesium_134"],label="Caesium_134",color="red")
sns.scatterplot(x=Inter_Data["PAYS"],y=Inter_Data["Caesium_137"],label="Caesium_137",color="yellow")
sns.scatterplot(x=Inter_Data["PAYS"],y=Inter_Data["Iodine_131"],label="Iodine_131",color="green")
plt.title('COUNTRY', fontsize=10)
plt.tight_layout()
plt.legend()
plt.show()


# In[105]:


figure = plt.figure(figsize=(20,8))
sns.boxplot(data=Inter_Data[["Caesium_134","Caesium_137","Iodine_131"]], width=.7, showfliers=False)
plt.title('Chemical', fontsize=10)
plt.legend()
plt.show()


# In[106]:


figure = plt.figure(figsize=(20,8))
Inter_Data["PAYS"].value_counts().plot.pie(autopct='%1.1f%%',shadow=True)
plt.title('Country', fontsize=10)
plt.tight_layout()
plt.show()


# In[107]:


Country_Data = Inter_Data.copy()
Country_Data = Country_Data[["PAYS","Caesium_134","Caesium_137","Iodine_131"]]


# In[108]:


Country_Data = Country_Data.groupby(["PAYS"]).sum()


# In[109]:


Country_Data


# In[110]:


axis_s = Country_Data.sort_values("PAYS", ascending=False).plot(kind='bar', subplots=False,
                 sharex=True, sharey=True, use_index=True,
                 legend=True, fontsize=15, stacked=True, rot=55,figsize=(20,8))
plt.title('Country', fontsize=10)
plt.tight_layout()
plt.show()


# In[111]:


Location_Data = Inter_Data.copy()
Location_Data = Location_Data[["Location","Caesium_134","Caesium_137","Iodine_131"]]
Location_Data = Location_Data.groupby(["Location"]).sum()
Location_Data = Location_Data.sort_values('Location', ascending=False)


# In[112]:


Location_Data


# In[113]:


axis_s = Location_Data.sort_values("Location", ascending=False).iloc[65:95].plot(kind='barh', subplots=False,
                 sharex=True, sharey=True, use_index=True,
                 legend=True, fontsize=15, stacked=True, rot=55,figsize=(25,25), y = ["Caesium_134","Caesium_137","Iodine_131"])
plt.title('Location', fontsize=10)
plt.tight_layout()
plt.show()


# In[114]:


import cartopy.crs as ccrs


# In[115]:


figure = plt.figure(figsize=(20,8))
Map_Plot = Basemap(projection="cyl",resolution="c")
Map_Plot.drawmapboundary(fill_color="w")
Map_Plot.drawcoastlines(linewidth=0.5)
Map_Plot.drawmeridians(range(0, 360, 20),linewidth=0.8)
Map_Plot.drawparallels([-66,-23,0.0,23,66],linewidth=0.8)
lon,lat = Map_Plot(Inter_Data["Longitude"],Inter_Data["Latitude"])
Map_Plot.scatter(lon,lat,marker="*",alpha=0.20,color="r",edgecolor="None")
plt.title("COORDINATES")


# In[116]:


figure = plt.figure(figsize=(20,8))
Map_Plot = Basemap(projection="cyl",resolution="c")
Map_Plot.drawmapboundary(fill_color="w")
Map_Plot.drawcoastlines(linewidth=0.5)
Map_Plot.drawmeridians(range(0, 360, 20),linewidth=0.8)
Map_Plot.drawparallels([-66,-23,0.0,23,66],linewidth=0.8)
lon,lat = Map_Plot(Inter_Data["Longitude"],Inter_Data["Latitude"])
Map_Plot.plot(lon,lat,marker="*",alpha=0.20,color="k")
plt.title("COORDINATES")


# In[117]:


figure = plt.figure(figsize=(17,12))
Map_Plot = Basemap(projection='cyl',llcrnrlat=10,llcrnrlon=10,urcrnrlat=90,urcrnrlon=75,resolution='c')
Map_Plot.bluemarble()
Map_Plot.drawcountries()
Map_Plot.scatter(Inter_Data.Longitude,Inter_Data.Latitude,edgecolor='none',color='r',alpha=0.6)
plt.title("LOCAL", fontsize=15)


# In[118]:


figure = plt.figure(figsize=(17,12))
Map_Plot = Basemap(projection='cyl',llcrnrlat=5,llcrnrlon=10,urcrnrlat=80,urcrnrlon=25,resolution='c')
Map_Plot.bluemarble()
Map_Plot.drawcountries()
Map_Plot.scatter(Inter_Data.Longitude,Inter_Data.Latitude,edgecolor='none',color='r',alpha=0.6)
plt.title("LOCAL", fontsize=15)


# In[119]:


figure = plt.figure(figsize=(17,12))
Map_Plot = Basemap(projection='cyl',llcrnrlat=-25,llcrnrlon=-10,urcrnrlat=80,urcrnrlon=85,resolution='c')
Map_Plot.bluemarble()
Map_Plot.drawcountries()
Map_Plot.scatter(Inter_Data.Longitude,Inter_Data.Latitude,edgecolor='none',color='r',alpha=0.6)
plt.title("LOCAL", fontsize=15)


# In[120]:


figure = plt.figure(figsize=(17,12))
Map_Plot = Basemap(projection='cyl',llcrnrlat=5,llcrnrlon=-10,urcrnrlat=80,urcrnrlon=85,resolution='c')
Map_Plot.bluemarble()
Map_Plot.drawcountries()
Map_Plot.scatter(Inter_Data.Longitude,Inter_Data.Latitude,edgecolor='none',color='r',alpha=0.6)
plt.title("LOCAL", fontsize=15)


# In[121]:


Danger_Caesium_134 = Inter_Data[Inter_Data["Caesium_134"] >= Inter_Data["Caesium_134"].mean()]
Danger_Caesium_137 = Inter_Data[Inter_Data["Caesium_137"] >= Inter_Data["Caesium_137"].mean()]
Danger_Iodine_131 = Inter_Data[Inter_Data["Iodine_131"] >= Inter_Data["Iodine_131"].mean()]


# In[123]:


Danger_Caesium_134 = Danger_Caesium_134.reset_index(drop=True)
Danger_Caesium_137 = Danger_Caesium_137.reset_index(drop=True)
Danger_Iodine_131 = Danger_Iodine_131.reset_index(drop=True)


# In[124]:


Danger_Caesium_134


# In[125]:


Danger_Caesium_137


# In[126]:


Danger_Iodine_131


# In[127]:


figure = plt.figure(figsize=(17,12))
Map_Plot = Basemap(projection='cyl',llcrnrlat=5,llcrnrlon=-10,urcrnrlat=80,urcrnrlon=85,resolution='c')
Map_Plot.bluemarble()
Map_Plot.drawcountries()
Map_Plot.scatter(Danger_Caesium_134.Longitude,Danger_Caesium_134.Latitude,edgecolor='none',color='r',alpha=0.6)
Map_Plot.scatter(Danger_Caesium_137.Longitude,Danger_Caesium_137.Latitude,edgecolor='none',color='y',alpha=0.3)
Map_Plot.scatter(Danger_Iodine_131.Longitude,Danger_Iodine_131.Latitude,edgecolor='none',color='g',alpha=0.9)
plt.title("CHEMICAL / DANGER POINTS", fontsize=15)


# In[128]:


figure = plt.figure(figsize=(17,12))
Map_Plot = Basemap(projection='cyl',llcrnrlat=5,llcrnrlon=-10,urcrnrlat=80,urcrnrlon=85,resolution='c')
Map_Plot.bluemarble()
Map_Plot.drawcountries()
Map_Plot.scatter(30.2219,51.2763,edgecolor='none',color='r',linewidths=1.5)
Map_Plot.scatter(Inter_Data.Longitude,Inter_Data.Latitude,edgecolor='none',color='y',alpha=0.6)
plt.title("CHERNOBYL", fontsize=15)


# In[129]:


figure = plt.figure(figsize=(13,8))
plt.subplot(projection="polar")
plt.grid(True)
plt.plot(Inter_Data.Longitude,Inter_Data.Latitude,color="y",label="CHEMICAL")
plt.tight_layout()
plt.legend(prop=dict(size=10))
plt.title("CHERNOBYL")
plt.show()


# In[130]:


figure = plt.figure(figsize=(17,15))
bar_plot = sns.barplot(x=Danger_Caesium_134["PAYS"], y=Danger_Caesium_134.index, lw=0)
plt.title("Caesium_134")
plt.show()


# In[132]:


figure = plt.figure(figsize=(17,15))
bar_plot = sns.barplot(x=Danger_Caesium_137["PAYS"], y=Danger_Caesium_137.index, lw=0)
plt.title("Caesium_137")
plt.show()


# In[133]:


figure = plt.figure(figsize=(17,15))
bar_plot = sns.barplot(x=Danger_Iodine_131["PAYS"], y=Danger_Iodine_131.index, lw=0)
plt.title("Iodine_131")
plt.show()

